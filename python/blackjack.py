
from card import CardSuit
from deck import FullDeckNoJokers


def test_same_suit() -> bool:
  deck = FullDeckNoJokers()
  deck.shuffle()

  same = False
  card_1 = deck.draw_one()
  card_2 = deck.draw_one()

  if card_1.suit == card_2.suit:
    same = True
  
  return same

if __name__ == "__main__":
  num_total_tests = 1000
  num_positive = 0
  for _ in range(num_total_tests):
    positive = test_same_suit()
    if positive:
      num_positive += 1

  print("Positive proportion: {}".format(float(num_positive) / float(num_total_tests)))
  print("Expected proportion: {}".format(float(12.0 / 51.0)))
