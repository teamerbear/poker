
import unittest

from card import CardSuit, CardValue, Card
from deck import FullDeckNoJokers


class BlackjackTest(unittest.TestCase):

  def test_deck_no_jokers(self):
    deck = FullDeckNoJokers()
    self.assertEqual(deck.cards_left(), deck.deck_size())

    # After a shuffle, the size should still be the same.
    deck.shuffle()
    self.assertEqual(deck.cards_left(), deck.deck_size())
    deck.shuffle()
    self.assertEqual(deck.cards_left(), deck.deck_size())
    deck.shuffle()
    self.assertEqual(deck.cards_left(), deck.deck_size())

    # After we draw a card, the deck shouldn't contain that card anymore.
    card = deck.draw_one()
    self.assertFalse(deck.contains(card))
    self.assertEqual(deck.cards_left(), deck.deck_size() - 1)

    # Put it back in the deck and check again.
    deck.insert_one(card)
    self.assertTrue(deck.contains(card))
    self.assertEqual(deck.cards_left(), deck.deck_size())

    # We shouldn't be able to insert any card into the deck because it's full.
    card = Card(CardSuit.SPADES, CardValue.TEN)
    self.assertRaises(ValueError, deck.insert_one, card)
    self.assertEqual(deck.cards_left(), deck.deck_size())
    card = Card(CardSuit.HEARTS, CardValue.JACK)
    self.assertRaises(ValueError, deck.insert_one, card)
    self.assertEqual(deck.cards_left(), deck.deck_size())
    card = Card(CardSuit.DIAMONDS, CardValue.THREE)
    self.assertRaises(ValueError, deck.insert_one, card)
    self.assertEqual(deck.cards_left(), deck.deck_size())

    # The cards left should be reflected after drawing.
    card_1 = deck.draw_one()
    self.assertEqual(deck.deck_size() - 1, deck.cards_left())
    card_2 = deck.draw_one()
    self.assertEqual(deck.deck_size() - 2, deck.cards_left())
    card_3 = deck.draw_one()
    self.assertEqual(deck.deck_size() - 3, deck.cards_left())

    # We shouldn't be able to insert any jokers into this deck.
    joker_1 = Card(CardSuit.JOKER_1, CardValue.JOKER_1)
    joker_2 = Card(CardSuit.JOKER_2, CardValue.JOKER_2)
    self.assertRaises(ValueError, deck.insert_one, joker_1)
    self.assertRaises(ValueError, deck.insert_one, joker_2)

    # Replace the cards we took out.
    deck.insert_one(card_1)
    deck.insert_one(card_2)
    deck.insert_one(card_3)
    self.assertEqual(deck.cards_left(), deck.deck_size())

    # Draw all the cards in the deck.
    self.assertEqual(52, deck.deck_size())
    for _ in range(52):
      _ = deck.draw_one()
    
    # Should raise an error when we try to draw a 53rd card.
    self.assertRaises(ValueError, deck.draw_one)


if __name__ == "__main__":
  unittest.main()