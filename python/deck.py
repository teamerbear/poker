
from random import randint

from card import CardSuit, CardValue, Card

class DeckBase(object):

  def __init__(self) -> None:
    self.cards = []
  
  def deck_size(self):
    return 52

  def cards_left(self):
    return len(self.cards)

  def insert_one(self, card: Card):
    # Don't insert if the deck is already full.
    if len(self.cards) >= self.deck_size():
      raise ValueError("Cannot insert card \'{}\' into deck, deck is full!".format(card.to_string()))

    # Don't insert if the card is already in the deck.
    if self.contains(card):
      raise ValueError("Cannot insert card \'{}\' into deck, already in deck!".format(card.to_string()))

  def draw_one(self) -> Card:
    card = None
    if self.cards_left() > 0:
      i = 0
      if self.cards_left() > 1:
        i = randint(0, len(self.cards) - 1)

      card = self.cards.pop(i)
    else:
      raise ValueError("Can't draw card from deck, deck is empty!")
    
    return card
  
  def contains(self, check_card: Card) -> bool:
    found = False
    if self.cards_left() > 0:
      for card in self.cards:
        if card.suit == check_card.suit:
          if card.value == check_card.value:
            found = True
            break

    return found
  
  def shuffle(self):
    for i in range(1000):
      card = self.draw_one()
      self.insert_one(card)


class FullDeckNoJokers(DeckBase):

  def __init__(self):
    super().__init__()
    for suit in CardSuit:
      if suit not in [CardSuit.JOKER_1, CardSuit.JOKER_2]:
        for val in CardValue:
          if val not in [CardValue.JOKER_1, CardValue.JOKER_2]:
            card = Card(suit, val)
            self.insert_one(card)

  def insert_one(self, card: Card):
    super().insert_one(card)
    
    # Don't insert if it's a joker.
    if card.suit in [CardSuit.JOKER_1, CardSuit.JOKER_2]:
      raise ValueError("Can't insert card \'{}\' into deck, no jokers allowed!".format(card.to_string()))

    self.cards.append(card)
    
class FullDeckWithJokers(DeckBase):

  def __init__(self):
    super().__init__()
    for suit in CardSuit:
      if suit not in [CardSuit.JOKER_1, CardSuit.JOKER_2]:
        for val in CardValue:
          if val not in [CardValue.JOKER_1, CardValue.JOKER_2]:
            card = Card(suit, val)
            self.insert_one(card)

    # Insert the two jokers last.
    joker_1 = Card(CardSuit.JOKER_1, CardValue.JOKER_1)
    joker_2 = Card(CardSuit.JOKER_2, CardValue.JOKER_2)
    self.insert_one(joker_1)
    self.insert_one(joker_2)
  
  def deck_size(self):
      return 54

  def insert_one(self, card: Card):
    super().insert_one(card)
    self.cards.append(card)
    