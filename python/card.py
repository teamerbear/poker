
from enum import Enum


class CardSuit(Enum):
  CLUBS = 1
  DIAMONDS = 2
  HEARTS = 3
  SPADES = 4
  JOKER_1 = 5
  JOKER_2 = 5


class CardValue(Enum):
  ACE = 1
  TWO = 2
  THREE = 3
  FOUR = 4
  FIVE = 5
  SIX = 6
  SEVEN = 7
  EIGHT = 8
  NINE = 9
  TEN = 10
  JACK = 11
  QUEEN = 12
  KING = 13
  JOKER_1 = 15
  JOKER_2 = 15


class Card(object):

  def __init__(self, suit: CardSuit, value: CardValue):
    self.suit = suit
    self.value = value
  
  def to_string(self) -> str:
    display_str = ""
    if self.suit in [CardSuit.JOKER_1, CardSuit.JOKER_2]:
      display_str = "JOKER"
    else: 
      display_str = "{} of {}".format(self.value.name, self.suit.name)
    
    return display_str

